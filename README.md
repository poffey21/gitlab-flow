# GitLab Flow

How much do we still support GitLab Flow?

* [x] `default`, `staging`, `production` branches are protected
* [x] all changes flow through the standard `feature-*` branches and are unprotected
* [x] MRs between `feature-*` --> `default` should auto-delete the branch upon merge
* [x] MRs between `default` --> `staging` and `default` --> `production` should NOT auto-delete the branch on merge (this works for MRs between two branches)
* [ ] If folks use GitLab Flow... why do we need Protected Environments???
* [ ] How do users effectively make built artifacts from `default` branch pipeline in the `staging`/`production` branches? (Generic Packages)
* [ ] Support for Release CLI
* [ ] Ideally, we would let FF Merges happen from `default` to other branches, but create Merge Commits for `feature-*` to `default`.
* [ ] Do we dogfood this today?


